import pandas as pd  
import numpy as np  
import matplotlib.pyplot as plt  
from sklearn.model_selection import train_test_split 
from sklearn.svm import SVR

X = np.arange(1,100).reshape(-1,1)
y = np.arange(1,297,3).ravel()

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
svr_poly = SVR(kernel='linear', C=100, gamma='auto', degree=3, epsilon=.1, coef0=1)
svr_poly.fit(X_train, y_train)
print("Score:" + str(svr_poly.score(X, y)))
plt.plot(X, svr_poly.predict(X))
plt.scatter(X_test, y_test, color='r')
plt.show()